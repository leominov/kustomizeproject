module github.com/leominov/KustomizeProject

go 1.13

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.7
	sigs.k8s.io/kustomize/kyaml v0.0.2
)
