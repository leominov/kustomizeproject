package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/leominov/KustomizeProject/pkg/handlers"
	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

const (
	location = ".config/kustomize/plugin/advert.tinkoff.ru/v1/kustomizeproject/"
	usage    = `Works only as Kustomize 3.0 plugin.

Available generators: %v
Available transformers: %v

Usage:
  $ KustomizeProject init
  $ kustomize build --enable_alpha_plugins ./`
)

func processKustomizeInput(args []string) error {
	if len(args) <= 1 {
		return fmt.Errorf(usage, handlers.GetGeneratorsNames(), handlers.GetTransformersNames())
	}
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	p, err := project.LoadFromFile(args[1], dir)
	if err != nil {
		return err
	}
	k, err := kustomize.LoadFromDirectory(dir)
	if err != nil {
		return err
	}
	b, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return err
	}
	if len(b) > 0 {
		return handlers.RunTransformers(dir, p, k, b)
	}
	return handlers.RunGenerators(dir, p, k)
}

func processCommandsInput(args []string) (bool, error) {
	if len(args) <= 1 {
		return false, nil
	}
	switch args[1] {
	case "init":
		return true, initCommand()
	}
	return false, nil
}

func realMain(args []string) error {
	if ok, err := processCommandsInput(args); ok {
		return err
	}
	return processKustomizeInput(args)
}

func main() {
	err := realMain(os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
