# Kustomize Project

Framework to customize specifications directory as a Project.

## Configuration

```yaml
---
apiVersion: advert.tinkoff.ru/v1
kind: KustomizeProject
metadata:
  name: project
settings:
  dependencies:
    - ../../infrastructure/redis/kustomize
    - ../serviceB/kustomize
  resources:
    - ../../postgres.Deployment.yaml
    - ../../postgres.Service.yaml
  argoCD:
    # For Kustomize applications
    applications:
      - ../redis.Application.yaml
```
