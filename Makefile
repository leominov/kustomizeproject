GIT_COMMIT := $(shell git rev-parse --short HEAD)

.PHONY: test
test:
	@go test ./... -cover

.PHONY: test-report
test-report:
	@go test ./... -coverprofile=coverage.txt && go tool cover -html=coverage.txt

install:
	@go build -o KustomizeProject ./
	@mv KustomizeProject ~/.config/kustomize/plugin/advert.tinkoff.ru/v1/kustomizeproject/

internal-release:
	@GOOS=linux go build -o KustomizeProject-$(GIT_COMMIT) ./
	@dope nexus upload -d KustomizeProject ./KustomizeProject-$(GIT_COMMIT) --force
	@rm -f KustomizeProject-$(GIT_COMMIT)
