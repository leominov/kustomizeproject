package main

import (
	"io/ioutil"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"
)

func initCommand() error {
	dir, err := homedir.Dir()
	if err != nil {
		return err
	}
	pluginDir := path.Join(dir, location)
	err = os.MkdirAll(pluginDir, 0755)
	if err != nil {
		return err
	}
	currentBin, err := os.Executable()
	if err != nil {
		return err
	}
	input, err := ioutil.ReadFile(currentBin)
	if err != nil {
		return err
	}
	destFile := path.Join(pluginDir, path.Base(currentBin))
	err = ioutil.WriteFile(destFile, input, 0755)
	if err != nil {
		return err
	}
	return nil
}
