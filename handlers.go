package main

import (
	_ "github.com/leominov/KustomizeProject/pkg/handlers/argocdapplications"
	_ "github.com/leominov/KustomizeProject/pkg/handlers/dependencies"
	_ "github.com/leominov/KustomizeProject/pkg/handlers/example"
	_ "github.com/leominov/KustomizeProject/pkg/handlers/resources"
)
