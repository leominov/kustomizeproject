package project

import (
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"

	"github.com/leominov/KustomizeProject/pkg/kustomize"
	yaml "gopkg.in/yaml.v2"
)

const (
	apiVersion              = "advert.tinkoff.ru/v1"
	kind                    = "KustomizeProject"
	defaultKustomizeBinPath = "kustomize"
)

type Project struct {
	APIVersion string   `yaml:"apiVersion"`
	Kind       string   `yaml:"kind"`
	Settings   Settings `yaml:"settings"`
}

type Settings struct {
	Example              bool     `yaml:"example"`
	KustomizeBinPath     string   `yaml:"kustomizeBinPath"`
	Dependencies         []string `yaml:"dependencies"`
	DependenciesExcluded []string `yaml:"excludeDependencies"`
	Resources            []string `yaml:"resources"`
	GenerateNameSuffix   string   `yaml:"generateNameSuffix"`
	ArgoCD               struct {
		Applications []string `yaml:"applications"`
	} `yaml:"argoCD"`
}

func loadProjectFile(filename string) (*Project, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	project := &Project{}
	if err := yaml.Unmarshal(b, project); err != nil {
		return nil, err
	}
	if len(project.Settings.KustomizeBinPath) == 0 {
		project.Settings.KustomizeBinPath = defaultKustomizeBinPath
	}
	return project, nil
}

func (p *Project) updateResourcePaths(workDir string) error {
	for i, dep := range p.Settings.Dependencies {
		dep = os.ExpandEnv(dep)
		absPath, err := filepath.Abs(path.Join(workDir, dep))
		if err != nil {
			return err
		}
		_, err = os.Stat(absPath)
		if err != nil {
			return err
		}
		p.Settings.Dependencies[i] = absPath
	}
	return nil
}

func LoadFromFile(filename string, workDir string) (*Project, error) {
	project, err := loadProjectFile(filename)
	if err != nil {
		return nil, err
	}
	return project, project.updateResourcePaths(workDir)
}

func DiscoverProjectInDirectory(dir string) (string, *Project, error) {
	kustomization, err := kustomize.LoadFromDirectory(dir)
	if err != nil {
		if os.IsNotExist(err) {
			return "", nil, nil
		}
		return "", nil, err
	}
	for _, gen := range kustomization.Generators {
		project, err := LoadFromFile(gen, dir)
		if err != nil {
			continue
		}
		if project.APIVersion == apiVersion && project.Kind == kind {
			return gen, project, nil
		}
	}
	return "", nil, nil
}

func (s *Settings) IsDependencyExcluded(dep string) bool {
	for _, mask := range s.DependenciesExcluded {
		re := regexp.MustCompile(mask)
		if re.MatchString(dep) {
			return true
		}
	}
	return false
}
