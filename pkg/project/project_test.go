package project

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFromFile(t *testing.T) {
	wd := "../../test_data/services/serviceA/"
	_, err := LoadFromFile("../../test_data/services/serviceA/not-found.yaml", wd)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	deps, err := LoadFromFile("../../test_data/services/serviceA/project.yaml", wd)
	if err != nil {
		t.Fatal(err)
	}
	if len(deps.Settings.Dependencies) != 2 {
		t.Errorf("Must be 2, but got %d", len(deps.Settings.Dependencies))
	}
}

func TestSettings_IsDependencyExcluded(t *testing.T) {
	s := &Settings{
		DependenciesExcluded: []string{
			"foobar",
			"foobar/raboof",
		},
	}
	assert.True(t, s.IsDependencyExcluded("foobar"))
	assert.True(t, s.IsDependencyExcluded("foobar/raboof"))
	assert.False(t, s.IsDependencyExcluded("barfoo"))
}
