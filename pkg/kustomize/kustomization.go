package kustomize

import (
	"io/ioutil"
	"os"
	"path"

	yaml "gopkg.in/yaml.v2"
)

const (
	KustomizationFilename = "kustomization.yaml"
)

type Kustomization struct {
	Resources    []string          `yaml:"resources,omitempty"`
	Generators   []string          `yaml:"generators,omitempty"`
	NamePrefix   string            `yaml:"namePrefix,omitempty"`
	CommonLabels map[string]string `yaml:"commonLabels,omitempty"`
}

func LoadFromDirectory(dir string) (*Kustomization, error) {
	filename := path.Join(dir, KustomizationFilename)
	_, err := os.Stat(filename)
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	kustomization := &Kustomization{}
	err = yaml.Unmarshal(b, kustomization)
	if err != nil {
		return nil, err
	}
	for i, res := range kustomization.Resources {
		kustomization.Resources[i] = path.Join(path.Dir(filename), res)
	}
	for i, res := range kustomization.Generators {
		kustomization.Generators[i] = path.Join(path.Dir(filename), res)
	}
	return kustomization, nil
}
