package kustomize

import (
	"fmt"
	"os/exec"
)

func Build(bin string, resources []string) ([]string, error) {
	var result []string
	for _, resource := range resources {
		args := []string{
			"build",
			// enable plugins, an alpha feature
			"--enable_alpha_plugins",
			// local kustomizations may load files from outside their root
			"--load_restrictor=LoadRestrictionsNone",
			resource,
		}
		out, err := exec.Command(bin, args...).CombinedOutput()
		if err != nil {
			return nil, fmt.Errorf("error building %q: %s", resource, string(out))
		}
		result = append(result, string(out))
	}
	return result, nil
}
