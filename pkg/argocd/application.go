package argocd

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/leominov/KustomizeProject/pkg/kustomize"

	"gopkg.in/yaml.v2"
)

type Application struct {
	Spec ApplicationSpec `yaml:"spec"`
}

type ApplicationSpec struct {
	Destination DestinationSpec `yaml:"destination"`
	Project     string          `yaml:"project"`
	Source      SourceSpec      `yaml:"source"`
}

type DestinationSpec struct {
	Namespace string `yaml:"namespace"`
	Server    string `yaml:"server"`
}

type SourceSpec struct {
	Path           string               `yaml:"path"`
	RepoURL        string               `yaml:"repoURL"`
	TargetRevision string               `yaml:"targetRevision"`
	Kustomize      *KustomizeSourceSpec `yaml:"kustomize"`
}

type KustomizeSourceSpec struct {
	NamePrefix   string            `yaml:"namePrefix"`
	CommonLabels map[string]string `yaml:"commonLabels"`
}

func LoadFromFile(filename string) (*Application, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	a := &Application{}
	return a, yaml.Unmarshal(b, a)
}

func (s *SourceSpec) ResourcePath() string {
	ref := "master"
	if len(s.TargetRevision) > 0 {
		ref = s.TargetRevision
	}
	return fmt.Sprintf("%s//%s?ref=%s", s.RepoURL, s.Path, ref)
}

func (a *Application) KustomizationSpec() *kustomize.Kustomization {
	k := &kustomize.Kustomization{
		Resources: []string{a.Spec.Source.ResourcePath()},
	}
	if a.Spec.Source.Kustomize != nil {
		k.NamePrefix = a.Spec.Source.Kustomize.NamePrefix
		k.CommonLabels = a.Spec.Source.Kustomize.CommonLabels
	}
	return k
}

func (a *Application) RenderWithKustomize(binPath string) ([]string, error) {
	dir, err := ioutil.TempDir("", "argocdapp")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(dir)
	b, err := yaml.Marshal(a.KustomizationSpec())
	if err != nil {
		return nil, err
	}
	err = ioutil.WriteFile(path.Join(dir, "kustomization.yaml"), b, 0644)
	if err != nil {
		return nil, err
	}
	return kustomize.Build(binPath, []string{dir})
}
