package argocd

import (
	"testing"
)

func TestLoadFromFile(t *testing.T) {
	_, err := LoadFromFile("test_data/not_found.yaml")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = LoadFromFile("test_data/application.yaml")
	if err != nil {
		t.Error(err)
	}
}

func TestResourcePath(t *testing.T) {
	tests := []struct {
		s *SourceSpec
		p string
	}{
		{
			s: &SourceSpec{
				Path:           "foobar",
				RepoURL:        "https://google.com",
				TargetRevision: "develop",
			},
			p: "https://google.com//foobar?ref=develop",
		},
	}
	for _, test := range tests {
		p := test.s.ResourcePath()
		if p != test.p {
			t.Errorf("Must be %q, but got %q", test.p, p)
		}
	}
}
