package dependencies

import (
	"path/filepath"
	"sync"

	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

type Resolver struct {
	maxDepth       int
	wd             string
	project        *project.Project
	localResources map[string]bool
	lock           *sync.RWMutex
	resources      map[string]bool
}

func NewResolver(wd string, p *project.Project, k *kustomize.Kustomization) (*Resolver, error) {
	r := &Resolver{
		wd:             wd,
		maxDepth:       3,
		resources:      make(map[string]bool),
		localResources: make(map[string]bool),
		lock:           new(sync.RWMutex),
		project:        p,
	}
	for _, res := range k.Resources {
		r.localResources[res] = true
	}
	r.setupResources(p)
	return r, nil
}

func (r *Resolver) Project() *project.Project {
	return r.project
}

func (r *Resolver) setupResources(project *project.Project) {
	for _, resource := range project.Settings.Dependencies {
		r.resources[resource] = true
	}
}

func (r *Resolver) Resolve() ([]string, error) {
	if len(r.resources) == 0 {
		return nil, nil
	}
	for resource := range r.resources {
		res, err := r.loadResources(resource, r.maxDepth)
		if err != nil {
			return nil, err
		}
		for _, res := range res {
			r.resources[res] = true
		}
	}
	var resources []string
	for res := range r.resources {
		if _, ok := r.localResources[res]; ok {
			continue
		}
		// Проверяем, не исключен ли ресурс глобально
		if r.project.Settings.IsDependencyExcluded(res) {
			continue
		}
		resources = append(resources, res)
	}
	return resources, nil
}

func (r *Resolver) getResources() map[string]bool {
	r.lock.Lock()
	defer r.lock.Unlock()
	return r.resources
}

func (r *Resolver) addResource(resource string) {
	r.lock.RLock()
	defer r.lock.RUnlock()
	r.resources[resource] = true
}

func (r *Resolver) loadResources(resource string, depth int) ([]string, error) {
	if depth == 0 {
		return []string{}, nil
	}
	baseDir := filepath.Dir(resource)
	filename, proj, err := project.DiscoverProjectInDirectory(baseDir)
	if err != nil {
		return nil, err
	}
	if len(filename) == 0 {
		return nil, nil
	}
	proj.Settings.Dependencies = append(proj.Settings.Dependencies, resource)
	resources := proj.Settings.Dependencies
	for _, resource := range proj.Settings.Dependencies {
		rs := r.getResources()
		if _, ok := rs[resource]; !ok {
			r.addResource(resource)
			depth--
			depResources, err := r.loadResources(resource, depth)
			if err != nil {
				return nil, err
			}
			resources = append(resources, depResources...)
		}
	}
	return resources, nil
}
