package dependencies

import (
	"github.com/leominov/KustomizeProject/pkg/handlers"
	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

const (
	handlerName = "dependencies"
)

func init() {
	handlers.RegisterGenerator(handlerName, handle)
}

func handle(workDir string, p *project.Project, k *kustomize.Kustomization) ([]string, error) {
	r, err := NewResolver(workDir, p, k)
	if err != nil {
		return nil, err
	}
	resources, err := r.Resolve()
	if err != nil {
		return nil, err
	}
	if len(resources) == 0 {
		return nil, nil
	}
	kBin := r.Project().Settings.KustomizeBinPath
	return kustomize.Build(kBin, resources)
}
