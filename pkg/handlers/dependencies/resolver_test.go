package dependencies

import (
	"path"
	"testing"

	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

func TestResolve(t *testing.T) {
	tests := []struct {
		filename  string
		resources int
		depth     int
	}{
		{
			filename:  "../../../test_data/services/serviceA/project.yaml",
			resources: 4,
			depth:     3,
		},
		{
			filename:  "../../../test_data/services/serviceB/project.yaml",
			resources: 4,
			depth:     3,
		},
		{
			filename:  "../../../test_data/services/serviceC/project.yaml",
			resources: 0,
			depth:     2,
		},
		{
			filename:  "../../../test_data/services/serviceD/project.yaml",
			resources: 3,
			depth:     2,
		},
		{
			filename:  "../../../test_data/services/serviceB/project.yaml",
			resources: 4,
			depth:     3,
		},
	}
	for _, test := range tests {
		dir := path.Dir(test.filename)
		p, err := project.LoadFromFile(test.filename, dir)
		if err != nil {
			t.Fatal(err)
		}
		k, err := kustomize.LoadFromDirectory(dir)
		if err != nil {
			t.Fatal(err)
		}
		r, err := NewResolver(dir, p, k)
		if err != nil {
			t.Fatal(err)
		}
		resources, err := r.Resolve()
		if err != nil {
			t.Fatal(err)
		}
		if len(resources) != test.resources {
			t.Errorf("Must be %d, but got %d", test.resources, len(resources))
		}
	}
}
