package handlers

import (
	"fmt"
	"os"
	"strings"

	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

const (
	SpecsSeparator = "---\n"
)

var (
	generators   = map[string]Generator{}
	transformers = map[string]Transformer{}
)

type (
	Generator   func(workDir string, p *project.Project, k *kustomize.Kustomization) ([]string, error)
	Transformer func(workDir string, p *project.Project, k *kustomize.Kustomization, b []byte) ([]byte, error)
)

func GetGeneratorsNames() (result []string) {
	for name := range generators {
		result = append(result, name)
	}
	return
}

func GetTransformersNames() (result []string) {
	for name := range transformers {
		result = append(result, name)
	}
	return
}

func RegisterGenerator(name string, fn Generator) {
	generators[name] = fn
}

func RegisterTransformer(name string, fn Transformer) {
	transformers[name] = fn
}

func RunGenerators(dir string, p *project.Project, k *kustomize.Kustomization) error {
	var resources []string
	for name, h := range generators {
		generatorResources, err := h(dir, p, k)
		if err != nil {
			return fmt.Errorf("generator %q error: %v", name, err)
		}
		resources = append(resources, generatorResources...)
	}
	_, err := fmt.Fprintln(os.Stdout, strings.Join(resources, SpecsSeparator))
	return err
}

func RunTransformers(dir string, p *project.Project, k *kustomize.Kustomization, b []byte) error {
	for name, h := range transformers {
		out, err := h(dir, p, k, b)
		if err != nil {
			return fmt.Errorf("transformer %q error: %v", name, err)
		}
		if out == nil {
			continue
		}
		b = out
	}
	_, err := fmt.Fprintln(os.Stdout, string(b))
	return err
}
