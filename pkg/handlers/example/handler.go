package example

import (
	"os"

	"github.com/leominov/KustomizeProject/pkg/handlers"
	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

const (
	handlerName     = "example"
	exampleResource = `apiVersion: apps/v1
kind: Deployment
metadata:
  name: example
  labels:
    user: ${USER}
spec:
  template:
    spec:
      containers:
        - name: example
          image: eu.gcr.io/project/group/example:latest`
)

func init() {
	handlers.RegisterGenerator(handlerName, handle)
}

func handle(_ string, p *project.Project, _ *kustomize.Kustomization) ([]string, error) {
	if !p.Settings.Example {
		return nil, nil
	}
	resource := os.ExpandEnv(exampleResource)
	return []string{resource}, nil
}
