package argocdapplications

import (
	"github.com/leominov/KustomizeProject/pkg/argocd"
	"github.com/leominov/KustomizeProject/pkg/handlers"
	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
)

const (
	handlerName = "argoCDApplications"
)

func init() {
	handlers.RegisterGenerator(handlerName, handle)
}

func handle(_ string, p *project.Project, _ *kustomize.Kustomization) ([]string, error) {
	apps, err := loadApplications(p.Settings.ArgoCD.Applications)
	if err != nil {
		return nil, err
	}
	var resources []string
	for _, app := range apps {
		rs, err := app.RenderWithKustomize(p.Settings.KustomizeBinPath)
		if err != nil {
			return nil, err
		}
		resources = append(resources, rs...)
	}
	return resources, nil
}

func loadApplications(paths []string) ([]*argocd.Application, error) {
	var apps []*argocd.Application
	for _, appPath := range paths {
		app, err := argocd.LoadFromFile(appPath)
		if err != nil {
			return nil, err
		}
		apps = append(apps, app)
	}
	return apps, nil
}
