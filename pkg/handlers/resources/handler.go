package resources

import (
	"fmt"
	"io/ioutil"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/leominov/KustomizeProject/pkg/handlers"
	"github.com/leominov/KustomizeProject/pkg/kustomize"
	"github.com/leominov/KustomizeProject/pkg/project"
	"sigs.k8s.io/kustomize/kyaml/yaml"
)

const (
	handlerName = "resources"
)

func init() {
	handlers.RegisterGenerator(handlerName, handle)
}

func handle(workDir string, p *project.Project, _ *kustomize.Kustomization) ([]string, error) {
	var resources []string
	for _, resourcePath := range p.Settings.Resources {
		resource, err := processResource(path.Join(workDir, resourcePath), p.Settings.GenerateNameSuffix)
		if err != nil {
			return nil, err
		}
		resources = append(resources, resource)
	}
	return resources, nil
}

func processResource(filename string, generateNameSuffix string) (string, error) {
	var suffix string
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	obj, err := yaml.Parse(string(b))
	if err != nil {
		return "", err
	}
	switch generateNameSuffix {
	case "timestamp":
		suffix = strconv.FormatInt(time.Now().Unix(), 10)
	}
	err = patchGenerateName(obj, suffix)
	if err != nil {
		return "", err
	}
	return obj.String()
}

func patchGenerateName(obj *yaml.RNode, suffix string) error {
	generateName, err := obj.Pipe(yaml.Lookup("metadata", "generateName"))
	if err != nil {
		return err
	}
	if generateName == nil {
		return nil
	}
	name, err := generateName.String()
	if err != nil {
		return err
	}
	_, err = obj.Pipe(yaml.Lookup("metadata"), yaml.Clear("generateName"))
	if err != nil {
		return err
	}
	name = strings.TrimSuffix(strings.TrimSpace(name), "-")
	if len(suffix) > 0 {
		name = fmt.Sprintf("%s-%s", name, suffix)
	}
	_, err = obj.Pipe(
		yaml.Lookup("metadata"),
		yaml.SetField("name", yaml.NewScalarRNode(name)),
	)
	return err
}
