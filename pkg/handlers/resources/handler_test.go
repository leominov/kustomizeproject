package resources

import (
	"strings"
	"testing"

	"sigs.k8s.io/kustomize/kyaml/yaml"
)

func TestHandler_patchGenerateName(t *testing.T) {
	tests := []struct {
		in   string
		pref string
		out  string
	}{
		{
			in: `---
apiVersion: batch/v1
kind: Job
metadata:
  generateName: migrations-`,
			pref: "rand",
			out: `apiVersion: batch/v1
kind: Job
metadata:
  name: migrations-rand
`,
		},
		{
			in: `---
apiVersion: batch/v1
kind: Job
metadata:
  generateName: migrations-`,
			pref: "",
			out: `apiVersion: batch/v1
kind: Job
metadata:
  name: migrations
`,
		},
		{
			in: `---
apiVersion: batch/v1
kind: Job
metadata:
  generateName: migrations`,
			pref: "rand",
			out: `apiVersion: batch/v1
kind: Job
metadata:
  name: migrations-rand
`,
		},
		{
			in: `---
apiVersion: batch/v1
kind: Job
metadata:
  name: migrations`,
			pref: "rand",
			out: `apiVersion: batch/v1
kind: Job
metadata:
  name: migrations
`,
		},
	}
	for _, test := range tests {
		obj, err := yaml.Parse(test.in)
		if err != nil {
			t.Fatal(err)
		}
		err = patchGenerateName(obj, test.pref)
		if err != nil {
			t.Fatal(err)
		}
		s, err := obj.String()
		if err != nil {
			t.Fatal(err)
		}
		if !strings.EqualFold(s, test.out) {
			t.Errorf("Must be %q, but got %q", test.out, s)
		}
	}
}

func TestHandler_processResource(t *testing.T) {
	_, err := processResource("test_data/job.yaml", "timestamp")
	if err != nil {
		t.Error(err)
	}
	_, err = processResource("test_data/deployment.yaml", "timestamp")
	if err != nil {
		t.Error(err)
	}
	_, err = processResource("test_data/invalid_yaml.yaml", "timestamp")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = processResource("test_data/invalid_spec.yaml", "timestamp")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = processResource("test_data/not_found.yaml", "timestamp")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}
